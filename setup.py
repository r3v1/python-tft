#!/usr/bin/env python

from distutils.core import setup
from pathlib import Path

from setuptools import find_packages

about = {}
here = Path(__file__).parent.absolute()
with open(here / "tft" / '__version__.py', 'r') as f:
    exec(f.read(), about)

with open('README.md', 'r') as f:
    readme = f.read()

packages = find_packages()
requires = [
    "numpy>=1.17.4",
    "pandas>=0.25.3",
    "scikit-learn>=0.22",
    "tensorflow-probability>=0.8.0",
    "tensorflow<2.7.0",
    "pyarrow",
    "matplotlib",
    "xarray",
]

# List additional groups of dependencies here (e.g. development
# dependencies). Users will be able to install these using the "extras"
# syntax, for example:
#
#   $ pip install sampleproject[dev]
#
# Similar to `install_requires` above, these must be valid existing
# projects.
extras_require = {  # Optional
    'dev': ['pydot'],
    # 'test': ['coverage'],
}

setup(
    name=about['__title__'],
    version=about['__version__'],
    description=about['__description__'],
    long_description=readme,
    long_description_content_type='text/markdown',
    author=about['__author__'],
    author_email=about['__author_email__'],
    url=about['__url__'],
    packages=packages,
    #package_data={'': ['LICENSE', 'NOTICE']},
    include_package_data=True,
    # Specify which Python versions you support. In contrast to the
    # 'Programming Language' classifiers above, 'pip install' will check this
    # and refuse to install the project if the version does not match. See
    # https://packaging.python.org/guides/distributing-packages-using-setuptools/#python-requires
    python_requires='>=3.7, <3.10',
    install_requires=requires,
    extras_require=extras_require,
    license=about['__license__'],
    zip_safe=False,
    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate you support Python 3. These classifiers are *not*
        # checked by 'pip install'. See instead 'python_requires' below.
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    project_urls={  # Optional
        'Bug Reports': 'https://gitlab.com/r3v1/python-tft/-/issues',
        'Source': 'https://github.com/google-research/google-research/tree/master/tft',
        'Paper link': 'https://arxiv.org/pdf/1912.09363.pdf',
    },
)
