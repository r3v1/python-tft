__title__ = 'python-tft'
__description__ = 'Temporal Fusion Transformers'
# __long_description = """Temporal Fusion Transformers for Interpretable Multi-horizon Time Series Forecasting
#
# Authors: Bryan Lim, Sercan Arik, Nicolas Loeff and Tomas Pfister
# Paper link: https://arxiv.org/pdf/1912.09363.pdf
#
# Package author: David Revillas (drevillas@pm.me)
# """
__url__ = 'https://gitlab.com/r3v1/python-tft'
__version__ = '0.0.5'
__author__ = 'David Revillas'
__author_email__ = 'drevillas@pm.me'
__license__ = 'Apache 2.0'
