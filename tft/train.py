import json
import os
from contextlib import redirect_stdout
from datetime import datetime

import numpy as np
import pandas as pd
import tensorflow.compat.v1 as tf

from .experiment import ExperimentConfig
from .hyperparam import HyperparamOptManager
from .tft_model import TemporalFusionTransformer
from .utils import numpy_normalised_quantile_loss, serialize


class Runner:
    """Experiment runner"""
    
    def __init__(self,
                 experiment: ExperimentConfig,
                 use_cudnn: bool = False,
                 num_repeats: int = 1,
                 testing_mode: bool = True):
        
        # Tensorflow configuration
        self.use_cudnn = use_cudnn
        
        # Experiment configuration
        self.num_repeats = num_repeats
        self.quantiles = experiment.quantiles
        self.formatter = experiment.make_data_formatter()
        self.df = experiment.raw_data
        self.model_folder = experiment.model_folder
        self.results_folder = experiment.results_folder
        
        self.fixed_params = {}
        self.params = {"model_folder": self.model_folder}
        
        self.testing_mode = testing_mode
        
        self.train_df, self.valid_df, self.test_df = pd.DataFrame(), pd.DataFrame(), pd.DataFrame()
        self.train_samples, self.valid_samples = None, None
    
    @property
    def tf_config(self):
        """
        Creates tensorflow config for graphs to run on CPU or GPU.

        Specifies whether to run graph on gpu or cpu and which GPU ID to use for multi
        GPU machines.

        Returns
        -------
        Tensorflow config.
        """
        
        if not self.use_cudnn:
            tf.experimental.output_all_intermediates(True)
            
            os.environ['CUDA_VISIBLE_DEVICES'] = '-1'  # for training on cpu
            tf_config = tf.ConfigProto(log_device_placement=False, device_count={'GPU': 0})
        
        else:
            os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
            os.environ['CUDA_VISIBLE_DEVICES'] = str(0)
            
            # Replace with logger
            # print('Selecting GPU ID={}'.format(gpu_id))
            
            tf_config = tf.ConfigProto(log_device_placement=False)
            tf_config.gpu_options.allow_growth = True
        
        return tf_config
    
    def print_status(self):
        print()
        
        # Print datasets status
        length = len(self.train_df) + len(self.valid_df) + len(self.test_df)
        msg = f"\n{'SET':>12}\t{'FROM':^25}   {'TO':^25}\t{'SAMPLES':>8}\t{'%':^6}\n"
        for s, df in zip(["Training", "Validation", "Testing"], [self.train_df, self.valid_df, self.test_df]):
            msg += f"{s:>12}" \
                   f"\t{df.index.min()} - {df.index.max()}" \
                   f"\t{len(df):>8}" \
                   f"\t{100 * len(df) / length:>05.2f}\n"
        print('=' * 18 + "\n1 - DATASET INFO\n" + '=' * 18)
        print(msg)
        
        # Print experiment setting values
        print('=' * 25 + "\n2 - EXPERIMENT SETTINGS\n" + '=' * 25)
        print(f'{"num_epochs":>18} = {self.fixed_params["num_epochs"]}')
        print(f'{"hidden_layer_size":>18} = {self.params["hidden_layer_size"]}')
        print(f'{"train_samples":>18} = {self.train_samples}')
        print(f'{"valid_samples":>18} = {self.valid_samples}')
        print(f'{"quantiles":>18} = {self.quantiles}')
        print()
        
        # Print TFT settings
        print('=' * 20 + "\n3 - TFT PARAMETERS\n" + '=' * 20)
        print(json.dumps(serialize(self.params), indent=4))
        print()
        
        # Tensorboard callback
        print('=' * 12 + "\n4 - STATUS\n" + '=' * 12)
        print(f'\n> tensorboard --logdir={self.model_folder / "logs"}\n')
        print()
    
    def prepare_data(self):
        if self.train_df.empty or self.valid_df.empty or self.test_df.empty:
            # Data loading
            self.train_df, self.valid_df, self.test_df = self.formatter.split_data(self.df)
            self.train_samples, self.valid_samples = self.formatter.get_num_samples_for_calibration()
            
            self.fixed_params = self.formatter.get_experiment_params()
            self.params = self.formatter.get_default_model_params()
            self.params["model_folder"] = self.model_folder
        
        # Parameter overrides for testing only! Small sizes used to speed up script
        if self.testing_mode:
            # TODO: replace with logger
            print(f"Running in testing mode")
            
            self.fixed_params["num_epochs"] = 25
            # self.params["hidden_layer_size"] = 5
            self.train_samples, self.valid_samples = 1000, 150
    
    def fit(self):
        default_keras_session = tf.keras.backend.get_session()
        
        # Data loading
        self.prepare_data()
        
        # Hyperparam manager
        opt_manager = HyperparamOptManager(self.params, self.fixed_params, self.model_folder)
        
        # Print status
        self.print_status()
        
        # Training
        best_loss = np.inf
        for i in range(self.num_repeats):
            print()
            print('=' * 19 + f"\n   ITERATION {i + 1}/{self.num_repeats}\n" + '=' * 19)
            print()
            
            tf.reset_default_graph()
            with tf.Graph().as_default(), tf.Session(config=self.tf_config) as sess:
                tf.keras.backend.set_session(sess)
                
                params = opt_manager.get_next_parameters()
                model = TemporalFusionTransformer(params, self.quantiles, use_cudnn=self.use_cudnn)
                
                if not model.training_data_cached():
                    model.cache_batched_data(self.train_df, "train", num_samples=self.train_samples)
                    model.cache_batched_data(self.valid_df, "valid", num_samples=self.valid_samples)
                
                sess.run(tf.global_variables_initializer())
                model.fit()
                
                val_loss = model.evaluate()
                
                if val_loss < best_loss:
                    opt_manager.update_score(params, val_loss, model)
                    best_loss = val_loss
                    
                    print('-' * 26 + "\nNEW TFT PARAMETERS FOUND\n" + '-' * 26)
                    print(f"Best loss: {best_loss:.5f}\n")
                    print(json.dumps(serialize(params)))
                    print()
                
                tf.keras.backend.set_session(default_keras_session)
        
        # Save model summary to file
        with open(self.model_folder / "summary.txt", "w") as fp:
            with redirect_stdout(fp):
                model.model.summary()
        
        # Save model plot
        # tf.keras.utils.plot_model(
        #     model.model,
        #     to_file=self.model_folder / "model.png",
        #     show_shapes=True,
        #     show_dtype=True,
        #     show_layer_names=True,
        #     rankdir='TB',
        #     layer_range=None,
        # )
    
    def test(self):
        # print(f"Lanzando conjunto test")
        default_keras_session = tf.keras.backend.get_session()
        
        # Data loading
        self.prepare_data()
        
        # Hyperparam manager
        opt_manager = HyperparamOptManager(self.params, self.fixed_params, self.model_folder)
        opt_manager.load_results()
        
        tf.reset_default_graph()
        with tf.Graph().as_default(), tf.Session(config=self.tf_config) as sess:
            tf.keras.backend.set_session(sess)
            
            # Get best paramaters
            best_params = opt_manager.get_best_params()
            
            # Initialize TFT with best parameters
            model = TemporalFusionTransformer(best_params, self.quantiles, use_cudnn=self.use_cudnn)
            
            # Loads weights
            model.load(self.model_folder)
            
            val_loss = model.evaluate(self.valid_df)
            print(f"Best validation loss = {val_loss}")
            
            def extract_numerical_data(data):
                """Strips out forecast time and identifier columns."""
                return data[[col for col in data.columns if col not in {"forecast_time", "identifier"}]]
            
            output_map = model.predict(self.test_df, return_targets=True)
            
            # Export results
            for k in output_map.keys():
                df = self.formatter.format_predictions(output_map[k])
                df.to_csv(self.results_folder / f"{k}.csv")
                
                if k != "targets":
                    q = float(k[1:]) / 100
                    loss = numpy_normalised_quantile_loss(extract_numerical_data(output_map["targets"]),
                                                          extract_numerical_data(output_map[k]),
                                                          q)
                    print(f"Normalised quantile loss for test data: {k} = {loss.mean():.4f}")
            
            tf.keras.backend.set_session(default_keras_session)
        
        print(f"Entrenamiento completado @ {datetime.now()}")
        
        # print("Params:")
        # for k in best_params:
        #     print(k, " = ", best_params[k])
        print()
        
        # print("Normalised Quantile Loss for Test Data: P50={}, P90={}".format(p50_loss.mean(), p90_loss.mean()))
