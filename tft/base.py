"""Default data formatting functions for experiments.

For new datasets, inherit form GenericDataFormatter and implement
all abstract functions.

These dataset-specific methods:
1) Define the column and input types for tabular dataframes used by model
2) Perform the necessary input feature engineering & normalisation steps
3) Reverts the normalisation for predictions
4) Are responsible for train, validation and test splits
"""

import abc
import enum
import json
from pathlib import Path
from typing import Union
from warnings import warn

import pandas as pd
from sklearn.preprocessing import StandardScaler, LabelEncoder

from .utils import get_single_col_by_input_type, extract_cols_from_data_type


class DataTypes(enum.IntEnum):
    """Defines numerical types of each column."""
    REAL_VALUED = 0
    CATEGORICAL = 1
    DATE = 2


class InputTypes(enum.IntEnum):
    """Defines input types of each column."""
    TARGET = 0
    OBSERVED_INPUT = 1
    KNOWN_INPUT = 2
    STATIC_INPUT = 3
    ID = 4  # Single column used as an entity identifier
    TIME = 5  # Single column exclusively used as a time index


class GenericDataFormatter(abc.ABC):
    """
    Abstract base class for all data formatters.

    User can implement the abstract methods below to perform dataset-specific
    manipulations.
    """
    
    def __init__(self, config_file: Union[Path, str]):
        # Opens configuration file
        config_file = Path(config_file)
        if not config_file.is_file():
            raise FileNotFoundError(f"{self.__class__.__name__}'s configuration file not found in {config_file}")
        
        with open(config_file, "r") as fp:
            self.config = json.load(fp)
            self.config = self.config["formatter"]
        
        self.identifiers = None
        self._real_scalers = None
        self._cat_scalers = None
        self._target_scaler = None
        self._num_classes_per_cat_input = None
    
    def set_scalers(self, df: pd.DataFrame):
        """Calibrates scalers using the data supplied."""
        print('Setting scalers with training data...')
        
        column_definitions = self.get_column_definition()
        id_column = get_single_col_by_input_type(InputTypes.ID, column_definitions)
        target_column = get_single_col_by_input_type(InputTypes.TARGET, column_definitions)
        
        # Extract identifiers in case required
        self.identifiers = list(df[id_column].unique())
        
        # Format real scalers
        real_inputs = extract_cols_from_data_type(DataTypes.REAL_VALUED,
                                                  column_definitions,
                                                  {InputTypes.ID, InputTypes.TIME})
        
        data = df[real_inputs].values
        self._real_scalers = StandardScaler().fit(data)
        self._target_scaler = StandardScaler().fit(df[[target_column]].values)  # used for predictions
        
        # Format categorical scalers
        categorical_inputs = extract_cols_from_data_type(DataTypes.CATEGORICAL,
                                                         column_definitions,
                                                         {InputTypes.ID, InputTypes.TIME})
        
        categorical_scalers = {}
        num_classes = []
        for col in categorical_inputs:
            # Set all to str so that we don't have mixed integer/string columns
            srs = df[col].apply(str)
            categorical_scalers[col] = LabelEncoder().fit(srs.values)
            num_classes.append(srs.nunique())
        
        # Set categorical scaler outputs
        self._cat_scalers = categorical_scalers
        self._num_classes_per_cat_input = num_classes
    
    def transform_inputs(self, df: pd.DataFrame) -> pd.DataFrame:
        """
        Performs feature transformations.

        This includes both feature engineering, preprocessing and normalisation.

        Parameters
        ----------
        df: pd.DataFrame
           Data frame to transform.

        Returns
        -------
        pd.DataFrame
            Transformed data frame.
        """
        output = df.copy()
        
        if self._real_scalers is None or self._cat_scalers is None:
            raise ValueError('Scalers have not been set!')
        
        column_definitions = self.get_column_definition()
        
        real_inputs = extract_cols_from_data_type(DataTypes.REAL_VALUED,
                                                  column_definitions,
                                                  {InputTypes.ID, InputTypes.TIME})
        categorical_inputs = extract_cols_from_data_type(DataTypes.CATEGORICAL,
                                                         column_definitions,
                                                         {InputTypes.ID, InputTypes.TIME})
        
        # Format real inputs
        output[real_inputs] = self._real_scalers.transform(df[real_inputs].values)
        
        # Format categorical inputs
        for col in categorical_inputs:
            string_df = df[col].apply(str)
            output[col] = self._cat_scalers[col].transform(string_df)
        
        return output
    
    def format_predictions(self, df: pd.DataFrame) -> pd.DataFrame:
        """
        Reverts any normalisation to give predictions in original scale.

        Parameters
        ----------
        df: pd.DataFrame
            Dataframe of model predictions.

        Returns
        -------
        pd.DataFrame
            Data frame of unnormalised predictions.
        """
        output = df.copy()
        
        column_names = df.columns
        
        for col in column_names:
            if col not in {'forecast_time', 'identifier'}:
                output[col] = self._target_scaler.inverse_transform(df[col].values.reshape(-1, 1))
        
        return output
    
    # @abc.abstractmethod
    def split_data(self, df: pd.DataFrame):
        """
        Splits data frame into training-validation-test data frames.
        This also calibrates scaling object, and transforms data for
        each split.

        Parameters
        ----------
        df: pd.DataFrame
        """
        # TODO: replace with logger
        print('Formatting train-valid-test splits.')
        
        def get_date_idx(s: dict):
            start = s["start"]
            end = s["end"]
            if start is not None and end is not None:
                idx = (index >= pd.Timestamp(start, tz=index.tz)) & (index < pd.Timestamp(end, tz=index.tz))
            elif start is None and end is not None:
                idx = index < pd.Timestamp(end, tz=index.tz)
            elif start is not None and end is None:
                idx = index >= pd.Timestamp(start, tz=index.tz)
            else:
                idx = index
            
            return idx
        
        if self.config["split"]["by"]:
            c = self.config["split"]["by"]
            index = pd.to_datetime(df[c].apply(str))
        else:
            index = pd.to_datetime(df.index)
        
        train_idx = get_date_idx(self.config["split"]["train"])
        valid_idx = get_date_idx(self.config["split"]["validation"])
        test_idx = get_date_idx(self.config["split"]["test"])
        
        train = df.loc[train_idx]
        valid = df.loc[valid_idx]
        test = df.loc[test_idx]
        
        # Sanity checks
        if len(train.index.intersection(valid.index)) > 0:
            warn(f"Training set and validation set shares {len(train.index.intersection(valid.index))} indexes")
        if len(train.index.intersection(test.index)) > 0:
            warn(f"Training set and test set shares {len(train.index.intersection(test.index))} indexes")
        if len(test.index.intersection(valid.index)) > 0:
            warn(f"Test set and validation set shares {len(test.index.intersection(valid.index))} indexes")
        
        self.set_scalers(train)
        
        return (self.transform_inputs(data) for data in [train, valid, test])
    
    @property
    # @abc.abstractmethod
    def _column_definition(self):
        """Defines order, input type and data type of each column."""
        cols = list(self.config["column_definition"])  # copy
        definitions = [(c["name"], eval(c["data"], {}, DataTypes), eval(c["input"], {}, InputTypes)) for c in cols]
        
        return definitions
    
    # @abc.abstractmethod
    def get_fixed_params(self) -> dict:
        """
        Defines the fixed parameters used by the model for training.

        Requires the following keys:
          'total_time_steps': Defines the total number of time steps used by TFT
          'num_encoder_steps': Determines length of LSTM encoder (i.e. history)
          'num_epochs': Maximum number of epochs for training
          'early_stopping_patience': Early stopping param for keras
          'multiprocessing_workers': # of cpus for data processing


        Returns
        -------
        dict
            A dictionary of fixed parameters, e.g.:

            fixed_params = {
              'total_time_steps': 252 + 5,
              'num_encoder_steps': 252,
              'num_epochs': 100,
              'early_stopping_patience': 5,
              'multiprocessing_workers': 5,
            }
        """
        return self.config["fixed_params"]
    
    # Shared functions across data-formatters
    @property
    def num_classes_per_cat_input(self):
        """
        Returns number of categories per relevant input.

        This is sequentially required for keras embedding layers.
        """
        return self._num_classes_per_cat_input
    
    def get_num_samples_for_calibration(self) -> tuple:
        """
        Gets the default number of training and validation samples.

        Use to sub-sample the data for network calibration and a value of -1 uses
        all available samples.

        Returns
        -------
        tuple
            Tuple of (training samples, validation samples)
        """
        return -1, -1
    
    def get_column_definition(self):
        """"Returns formatted column definition in order expected by the TFT."""
        
        column_definition = self._column_definition
        
        # Sanity checks first.
        # Ensure only one ID and time column exist
        def _check_single_column(input_type):
            length = len([tup for tup in column_definition if tup[2] == input_type])
            
            if length != 1:
                raise ValueError('Illegal number of inputs ({}) of type {}'.format(length, input_type))
        
        _check_single_column(InputTypes.ID)
        _check_single_column(InputTypes.TIME)
        
        identifier = [tup for tup in column_definition if tup[2] == InputTypes.ID]
        time = [tup for tup in column_definition if tup[2] == InputTypes.TIME]
        real_inputs = [
            tup for tup in column_definition if tup[1] == DataTypes.REAL_VALUED and
                                                tup[2] not in {InputTypes.ID, InputTypes.TIME}
        ]
        categorical_inputs = [
            tup for tup in column_definition if tup[1] == DataTypes.CATEGORICAL and
                                                tup[2] not in {InputTypes.ID, InputTypes.TIME}
        ]
        
        return identifier + time + real_inputs + categorical_inputs
    
    def _get_input_columns(self):
        """Returns names of all input columns."""
        return [tup[0] for tup in self.get_column_definition() if tup[2] not in {InputTypes.ID, InputTypes.TIME}]
    
    def _get_tft_input_indices(self):
        """Returns the relevant indexes and input sizes required by TFT."""
        
        # Functions
        def _extract_tuples_from_data_type(data_type, defn):
            return [tup for tup in defn if tup[1] == data_type and tup[2] not in {InputTypes.ID, InputTypes.TIME}]
        
        def _get_locations(input_types, defn):
            return [i for i, tup in enumerate(defn) if tup[2] in input_types]
        
        # Start extraction
        column_definition = [
            tup for tup in self.get_column_definition()
            if tup[2] not in {InputTypes.ID, InputTypes.TIME}
        ]
        
        categorical_inputs = _extract_tuples_from_data_type(DataTypes.CATEGORICAL, column_definition)
        real_inputs = _extract_tuples_from_data_type(DataTypes.REAL_VALUED, column_definition)
        
        locations = {
            'input_size':
                len(self._get_input_columns()),
            'output_size':
                len(_get_locations({InputTypes.TARGET}, column_definition)),
            'category_counts':
                self.num_classes_per_cat_input,
            'input_obs_loc':
                _get_locations({InputTypes.TARGET}, column_definition),
            'static_input_loc':
                _get_locations({InputTypes.STATIC_INPUT}, column_definition),
            'known_regular_inputs':
                _get_locations({InputTypes.STATIC_INPUT, InputTypes.KNOWN_INPUT}, real_inputs),
            'known_categorical_inputs':
                _get_locations({InputTypes.STATIC_INPUT, InputTypes.KNOWN_INPUT}, categorical_inputs),
        }
        
        return locations
    
    def get_experiment_params(self):
        """Returns fixed model parameters for experiments."""
        
        required_keys = [
            'total_time_steps',
            'num_encoder_steps',
            'num_epochs',
            'early_stopping_patience',
            'multiprocessing_workers'
        ]
        
        fixed_params = self.get_fixed_params()
        
        for k in required_keys:
            if k not in fixed_params:
                raise ValueError('Field {}'.format(k) + ' missing from fixed parameter definitions!')
        
        fixed_params['column_definition'] = self.get_column_definition()
        
        fixed_params.update(self._get_tft_input_indices())
        
        return fixed_params
    
    def get_default_model_params(self):
        """Returns default optimised model parameters."""
        
        return self.config["model_params"]
