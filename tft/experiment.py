"""
Default configs for TFT experiments.

Contains the default output paths for data, serialised models and predictions
for the main experiments used in the publication.
"""
import abc
import json
from pathlib import Path
from typing import Union

import pandas as pd

from tft.base import GenericDataFormatter


class ExperimentConfig:
    """
    Defines experiment configs and paths to outputs.

    Attributes
    ----------
    root_folder: str, Path
        Root folder to contain all experimental outputs.
    experiment: str
        Name of experiment to run.
    data_folder: Path
        Folder to store data for experiment.
    model_folder: Path
        Folder to store serialised models.
    results_folder: Path
        Folder to store results.
    data_path: Path
        Path to primary data csv file used in experiment.
    hyperparam_iterations: int
        Default number of random search iterations for experiment.
    """

    def __init__(self, config_file: Union[Path, str]):
        """
        Creates configs based on default experiment chosen.

        Parameters
        ----------
        experiment: str
            Name of experiment.
        root_folder: str, Path
            Root folder to save all outputs of training.
        quantiles: list
            1D quantile list
        """
        # Opens configuration file
        config_file = Path(config_file)
        if not config_file.is_file():
            raise FileNotFoundError(f"{self.__class__.__name__}'s configuration file not found in {config_file}")

        with open(config_file, "r") as fp:
            self.config = json.load(fp)["experiment"]

        self.quantiles = self.config["quantiles"]

        self.root_folder = Path(self.config["root_folder"]).expanduser().absolute()
        self.experiment = self.config["name"]

        self.data_folder = self.root_folder / 'data' / self.experiment
        self.model_folder = self.root_folder / 'saved_models' / self.experiment
        self.results_folder = self.root_folder / 'results' / self.experiment

        # Creates folders if they don't exist
        self.data_folder.mkdir(parents=True, exist_ok=True)
        self.model_folder.mkdir(parents=True, exist_ok=True)
        self.results_folder.mkdir(parents=True, exist_ok=True)

    @property
    def raw_data(self) -> pd.DataFrame:
        """Reads raw data and returns DataFrame"""
        path = self.data_folder / self.config["raw_data_filename"]
        if not path.is_file():
            raise FileNotFoundError(path)
        
        if path.name.endswith(".csv"):
            parse_dates = self.config["parse_dates"] if "parse_dates" in self.config.keys() else None
            df = pd.read_csv(path, index_col=0, parse_dates=parse_dates)
        elif path.name.endswith(".parquet"):
            df = pd.read_parquet(path)
        else:
            raise ValueError(f"Not recognized format: {path.name}")

        return df

    @property
    def hyperparam_iterations(self) -> int:
        """Default number of random search iterations for experiment."""
        return self.config["hyperparam_iterations"]

    @abc.abstractmethod
    def make_data_formatter(self) -> GenericDataFormatter:
        """
        Gets a data formatter object for experiment.

        Returns
        -------
        GenericDataFormatter
            Default DataFormatter per experiment.
        """
        raise NotImplementedError()
