# Temporal Fusion Transformers for Interpretable Multi-horizon Time Series Forecasting

**NOTE: The original code is part of the publication [*Temporal Fusion Transformers for Interpretable Multi-horizon Time
Series Forecasting*](https://arxiv.org/pdf/1912.09363.pdf) by Bryan Lim, Sercan Arik, Nicolas Loeff and Tomas Pfister,
hosted in the [Google research repository](https://github.com/google-research/google-research/tree/master/tft).**

## Why another TFT repo?

The main reason for modifying the structure of the project was to adapt it to my needs and make it more generic, so that
it would be easier and faster to add a new dataset. In addition, some things have been polished, such as compatibility
with TensorFlow 1.x among other things.

### Main changes

- The creation of new GenericFormatters classes is abstracted as well as adding information to the experiment
  configuration of the main library (see `example.py`).
- The number of abstract methods to implement has been reduced thanks to the JSON configuration file that can be
  provided to these objects. Now, it is easier to customize the experiment setup (see `example.json`).
- Now, all experiments are included in a Runner object, in charge of launching training, testing and parameter
  optimization. No more scripts.

# Installing

```
pip install -e git+https://gitlab.com/r3v1/python-tft.git#egg=python-tft
```

# Running experiments

In this example it is shown the *volatility* experiment.

## JSON template

All of these parts must be included in a single JSON file like:

    {
      "formatter" {...},
      "experiment": {...}
    }

## Formatter

Configuration for the `GenericFormatter`.

- `column_definition`: List with each of the column definition to use. Each item will contain:
    - `name`: The column name as it appears in the dataset.
    - `data`: The input data type. Available types: `REAL_VALUED`, `CATEGORICAL`, `DATE`.
    - `input`: The numeric data type. Available types: `TARGET`, `OBSERVED_INPUT`, `KNOWN_INPUT`,
      `STATIC_INPUT`, `ID`, `TIME`.
- `fixed_params`: Defines the fixed parameters used by the model for training.
    - `total_time_steps`: Defines the total number of time steps used by TFT.
    - `num_encoder_steps`: Determines length of LSTM encoder (i.e. history).
    - `num_epochs`: Maximum number of epochs for training.
    - `early_stopping_patience`: Early stopping param for keras.
    - `multiprocessing_workers`: # of cpus for data processing.
- `model_params`: Returns default optimised model parameters.
    - `dropout_rate`: Dropout discard rate (`float`).
    - `hidden_layer_size`: Internal state size of TFT (`int`).
    - `learning_rate`: ADAM initial learning rate (`float`).
    - `minibatch_size`: Minibatch size for training (`int`).
    - `max_gradient_norm`: Max norm for gradient clipping (`float`).
    - `num_heads`: Number of heads for multi-head attention (`int`).
    - `stack_size`: Number of stacks (default 1 for interpretability) (`int`).
- `split`: Splits data in training, validation and testing sets.
    - `by`: Defines the index column. If `null`, the `index_col=0` parameter will be passed to `pd.read_csv` and
      dataframe's index will be used as index column to split sets.
    - `train`, `validation`, `test`: Defines `start` and `end` timestamp to split by.

### Template

```json
{
  "formatter": {
    "column_definition": [
      {
        "name": "Symbol",
        "data": "CATEGORICAL",
        "input": "ID"
      },
      {
        "name": "date",
        "data": "DATE",
        "input": "TIME"
      },
      {
        "name": "log_vol",
        "data": "REAL_VALUED",
        "input": "TARGET"
      },
      {
        "name": "open_to_close",
        "data": "REAL_VALUED",
        "input": "OBSERVED_INPUT"
      },
      {
        "name": "month",
        "data": "REAL_VALUED",
        "input": "KNOWN_INPUT"
      },
      {
        "name": "Region",
        "data": "CATEGORICAL",
        "input": "STATIC_INPUT"
      }
    ],
    "fixed_params": {
      "total_time_steps": 257,
      "num_encoder_steps": 252,
      "num_epochs": 100,
      "early_stopping_patience": 5,
      "multiprocessing_workers": 5
    },
    "model_params": {
      "dropout_rate": 0.3,
      "hidden_layer_size": 160,
      "learning_rate": 0.01,
      "minibatch_size": 64,
      "max_gradient_norm": 0.01,
      "num_heads": 1,
      "stack_size": 1
    },
    "split": {
      "by": null,
      "train": {
        "start": null,
        "end": "2015-12-31"
      },
      "validation": {
        "start": "2016-01-01",
        "end": "2017-12-31"
      },
      "test": {
        "start": "2018-01-01",
        "end": "2019-06-28"
      }
    }
  }
}
```

## Experiment configuration

Configuration for the `ExperimentConfig`.

- `name`: Experiment name. It will make directories under `root_folder` with that name.
- `root_folder`: Root folder to contain all experimental outputs.
- `raw_data_filename`: Name of the CSV or Parquet file.
- `parse_dates`: Parse dates of the CSV file (if provided). It will be passed to the `pd.read_csv` function.
- `quantiles`: List of quantiles to process.
- `hyperparam_iterations`: Default number of random search iterations for experiment.

### Template

```json
{
  "experiment": {
    "name": "volatility",
    "root_folder": "~/tft_output",
    "raw_data_filename": "formatted_omi_vol.parquet",
    "parse_dates": null,
    "quantiles": [
      0.1,
      0.5,
      0.9
    ],
    "hyperparam_iterations": 60
  }
}
```

## Experiment example implementation

Start by importing modules:

```python
from pathlib import Path
from typing import Union

from sklearn.preprocessing import StandardScaler, LabelEncoder

from tft.base import GenericDataFormatter, DataTypes, InputTypes
from tft.experiment import ExperimentConfig
from tft.train import Runner
from tft.utils import get_single_col_by_input_type, extract_cols_from_data_type
```

Subclass a *GenericFormatter*:

```python
class VolatilityFormatter(GenericDataFormatter):
    def __init__(self, config_file: Union[Path, str]):
        """Initialises formatter."""
        super(VolatilityFormatter, self).__init__(config_file=config_file)
        self.identifiers = None
        self._real_scalers = None
        self._cat_scalers = None
        self._target_scaler = None
        self._num_classes_per_cat_input = None

    def set_scalers(self, df):
        column_definitions = self.get_column_definition()
        id_column = get_single_col_by_input_type(InputTypes.ID, column_definitions)
        target_column = get_single_col_by_input_type(InputTypes.TARGET, column_definitions)

        # Extract identifiers in case required
        self.identifiers = list(df[id_column].unique())

        # Format real scalers
        real_inputs = extract_cols_from_data_type(
            DataTypes.REAL_VALUED, column_definitions,
            {InputTypes.ID, InputTypes.TIME})

        data = df[real_inputs].values
        self._real_scalers = StandardScaler().fit(data)
        self._target_scaler = StandardScaler().fit(df[[target_column]].values)  # used for predictions

        # Format categorical scalers
        categorical_inputs = extract_cols_from_data_type(
            DataTypes.CATEGORICAL, column_definitions,
            {InputTypes.ID, InputTypes.TIME})

        categorical_scalers = {}
        num_classes = []
        for col in categorical_inputs:
            # Set all to str so that we don't have mixed integer/string columns
            srs = df[col].apply(str)
            categorical_scalers[col] = LabelEncoder().fit(srs.values)
            num_classes.append(srs.nunique())

        # Set categorical scaler outputs
        self._cat_scalers = categorical_scalers
        self._num_classes_per_cat_input = num_classes

    def transform_inputs(self, df):
        output = df.copy()

        if self._real_scalers is None or self._cat_scalers is None:
            raise ValueError('Scalers have not been set!')

        column_definitions = self.get_column_definition()

        real_inputs = extract_cols_from_data_type(
            DataTypes.REAL_VALUED, column_definitions,
            {InputTypes.ID, InputTypes.TIME})
        categorical_inputs = extract_cols_from_data_type(
            DataTypes.CATEGORICAL, column_definitions,
            {InputTypes.ID, InputTypes.TIME})

        # Format real inputs
        output[real_inputs] = self._real_scalers.transform(df[real_inputs].values)

        # Format categorical inputs
        for col in categorical_inputs:
            string_df = df[col].apply(str)
            output[col] = self._cat_scalers[col].transform(string_df)

        return output

    def format_predictions(self, predictions):
        output = predictions.copy()

        column_names = predictions.columns

        for col in column_names:
            if col not in {'forecast_time', 'identifier'}:
                output[col] = self._target_scaler.inverse_transform(predictions[col].values.reshape(-1, 1))

        return output

```

Subclass a *ExperimentConfig*:

```python
class VolatilityExp(ExperimentConfig):
    def __init__(self, config_file: Union[Path, str]):
        self.config_file = config_file
        super(VolatilityExp, self).__init__(config_file)

    def make_data_formatter(self):
        return VolatilityFormatter(self.config_file)

```

#### Running experiment

```python
exp = VolatilityExp("example.json")

runner = Runner(exp, use_gpu=True)
runner.fit()
runner.test()
```
